## Casos de Prueba
### Crear padre e hijos correctamente
  
  En Postman:

  <img src="/uploads/8ebb2fafc8812bc69dc7b432ad59a051/image.png" width="500"/>

En Base de Datos se comprueba que se hayan guardado:

<img src="/uploads/9cdb0104b3e8fe6b732929a03101246c/image.png" width="300"/>

### Crea padre, pero los hijos son null

En Postman:

<img src="/uploads/56e39451bd51c771d862539cab226215/image.png" width="400"/>

En Base de Datos se comprueba que no se hayan guardado:

<img src="/uploads/6f6e5c3ebb25ae1f47dc50ccb934f761/image.png" width="300"/>
<img src="/uploads/949da24d3e5fac4bb99ac4c1ef2e3fd2/image.png" width="300"/>

En el servidor se da el error:
```bash
{
 {
    message: "ER_BAD_NULL_ERROR: Column 'nombre' cannot be null",
    code: 'ER_BAD_NULL_ERROR',
    errno: 1048,
    sqlMessage: "Column 'nombre' cannot be null",
    sqlState: '23000',
    index: 0,
    sql: 'INSERT INTO `hijo`(`id`, `nombre`, `padreId`) VALUES (DEFAULT, NULL, 1)',
    name: 'QueryFailedError',
    query: 'INSERT INTO `hijo`(`id`, `nombre`, `padreId`) VALUES (DEFAULT, ?, ?)',
    parameters: [ null, 1 ]
  },
  mensaje: 'Error al crear padre e hijos',
  data: {
    padre: { nombre: 'Soy tu padre', id: 1 },
    hijos: [ [Object], [Object] ]
  }
}

```
### El caso anterior sin usar el `transaccionEntityManager`:

Código:

```typescript
async registrarPadreConHijosTransaccion (padre: PadreEntity, hijos: HijoEntity[])
        : Promise<{mensaje: string, error: boolean, statusCode: number}> {
        return getManager('default')
            .transaction(
                async (transaccionEntityManager: EntityManager) => {
                    try{
                        // const padreRepository = transaccionEntityManager.getRepository(PadreEntity);
                        // const hijoRepository = transaccionEntityManager.getRepository(HijoEntity);
                        const padreCreado = await this._padreRepository.save(padre);
                        const hijosDelPadre = hijos.map(
                            (hijoParametro: HijoEntity) => {
                                hijoParametro.padre = padreCreado;
                                return hijoParametro;
                            }
                        );
                        await this._hijoRepository.save(hijosDelPadre);
                        return {
                            mensaje: 'Padre e hijos creados correctamente',
                            error: false,
                            statusCode: HttpStatus.CREATED,
                        }
                    } catch (error) {
                        console.error(
                            {
                                error,
                                mensaje: 'Error al crear padre e hijos',
                                data: {
                                    padre,
                                    hijos
                                },
                            },
                        );
                        throw new InternalServerErrorException({
                            mensaje: 'Error del servidor',
                        });
                    }
                }
            )
    }
```


En Postman:

<img src="/uploads/92534e7637affb9cadf2430797fe8e1e/image.png" width="400"/>

En Base de Datos se comprueba que no se hayan guardado:

<img src="/uploads/33355c9ea1cda58761a0f2cfbc28ccd1/image.png" width="300"/>
<img src="/uploads/a6948a9baf06c21b1b8c84c00a898691/image.png" width="250"/>

_Se comprueba que no se hizo correctamente el rollback_