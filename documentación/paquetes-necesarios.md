## Lista de programas usados
* Webstorm 2019.3.2
* Docker v19.03.8
* Postman v7.20.1
* DBeaver community v6.3.3
* Kitematic v0.17.10

## Lista de paquetes necesarios y sus versiones
* nodejs v12.14.1
* npm v6.13.4
* nestjs v6.7.2
* nestjs/typeorm v6.3.4 
* typescript v3.6.3
* typeorm v0.2.24