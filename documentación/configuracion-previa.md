# Configuración previa
* Se usa la siguiente configuración de base de datos Mysql:
```typescript
export const CONFIG_MYSQL: MysqlConnectionOptions = {
    type: 'mysql',
    host: 'localhost',
    port: 32773,
    name: 'default',
    username: 'root',
    password: 'root',
    database: 'db-ejemplo',
    entities: [
        PadreEntity,
        HijoEntity
    ],
    synchronize: true,
    dropSchema: true,
};
```
* El archivo `padre.entity.ts` posee los siguientes atributos y relaciones del padre:
```typescript
@Entity('padre')
export class PadreEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column(
        {
            type: 'varchar',
            name: 'nombre',
            length: 80
        }
    )
    nombre: string = null;

    @OneToMany(
        type => HijoEntity,
        hijo => hijo.padre
    )
    hijos: HijoEntity [];

}
```
* El archivo `hijo.entity.ts` posee los siguientes atributos y relaciones del hijo:
```typescript
@Entity('hijo')
export class HijoEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column(
        {
            type: 'varchar',
            name: 'nombre',
            length: 80
        }
    )
    nombre: string = null;

    @ManyToOne(
        type => PadreEntity,
        padre => padre.hijos,
        {
            nullable: false
        }
    )
    padre: PadreEntity;
}
```
* En los módulos se debe importar el Typeorm para cada entidad.

  Ejemplo del `hijo.module.ts`:
  ```typescript
  imports: [
        TypeOrmModule.forFeature([HijoEntity], 'default')],
  ```
* En cada servicio se debe inyectar el repositorio de la entidad
  
  Ejemplo del `hijo.service.ts`:
  ```typescript
  constructor(
        @InjectRepository(HijoEntity)
                private readonly _papaRepository: Repository<HijoEntity>) {
    }
  ```