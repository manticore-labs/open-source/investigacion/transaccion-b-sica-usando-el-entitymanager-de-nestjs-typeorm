import { Test, TestingModule } from '@nestjs/testing';
import { HijoController } from './hijo.controller';

describe('Hijo Controller', () => {
  let controller: HijoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HijoController],
    }).compile();

    controller = module.get<HijoController>(HijoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
