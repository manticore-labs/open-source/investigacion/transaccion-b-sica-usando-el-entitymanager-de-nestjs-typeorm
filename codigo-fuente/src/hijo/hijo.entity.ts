import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {PadreEntity} from "../padre/padre.entity";

@Entity('hijo')
export class HijoEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column(
        {
            type: 'varchar',
            name: 'nombre',
            length: 80
        }
    )
    nombre: string = null;

    @ManyToOne(
        type => PadreEntity,
        padre => padre.hijos,
        {
            nullable: false
        }
    )
    padre: PadreEntity;

}