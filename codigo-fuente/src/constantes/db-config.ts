import {MysqlConnectionOptions} from 'typeorm/driver/mysql/MysqlConnectionOptions';
import {PadreEntity} from "../padre/padre.entity";
import {HijoEntity} from "../hijo/hijo.entity";

export const CONFIG_MYSQL: MysqlConnectionOptions = {
    type: 'mysql',
    host: 'localhost',
    port: 32773,
    name: 'default',
    username: 'root',
    password: 'root',
    database: 'db-ejemplo',
    entities: [
        PadreEntity,
        HijoEntity
    ],
    synchronize: true,
    dropSchema: true,
};