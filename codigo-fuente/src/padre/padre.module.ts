import {Module} from '@nestjs/common';
import {PadreService} from './padre.service';
import {PadreController} from './padre.controller';
import {TypeOrmModule} from '@nestjs/typeorm';
import {PadreEntity} from './padre.entity';

@Module({
    imports: [TypeOrmModule.forFeature([PadreEntity], 'default')],
    providers: [PadreService],
    controllers: [PadreController],
    exports: [PadreService]
})
export class PadreModule {
}
