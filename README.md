# Transacción usando el EntityManager de Nestjs/Typeorm
Para realizar una transacción con Typeorm se usa el `Connection` o el `EntityManager`.
Aquí veremos cómo hacer una transacción básica usando el `EntityManager`.

## Índice  
1. [Paquetes necesarios](#paquetes-necesarios)  
2. [Sintaxis](#sintaxis)
3. [Ejemplo](#ejemplo)

   3.1. [Configuración previa](#configuración-previa) 

   3.2. [Pasos para realizar la transacción](#pasos-para-realizar-la-transacción)

4. [Casos de prueba](#casos-de-prueba)

## Paquetes necesarios
Para realizar la transacción primero debes tener un proyecto de Nestjs e instalar los paquetes, librerías, etc. 
necesarios. [Aquí la lista](documentación/paquetes-necesarios.md)

## Sintaxis
Para crear una transacción con `EntityManager` se emplea la siguiente sintaxis:
```typescript 
import {getManager} from "typeorm";

await getManager().transaction(async transactionalEntityManager => {
    
});
```

## Ejemplo
Para el ejemplo se usa una entidad padre relación 1 a muchos con una entidad hija.

![image](/uploads/b12dafebba35885cf4b8b84e59dbb977/image.png)

La transacción de ejemplo consiste en crear un padre con varios hijos.

### Configuración previa
La configuración de base de datos, servicios, modulos y entities se detalla en el siguiente archivo: [Aquí](documentación/configuracion-previa.md)

### Pasos para realizar la transacción
* En el `padre.controller.ts` se crea un método POST que recibe tanto al padre como a los hijos en el body y ejecuta la trasacción.

```typescript 
@Post('crear-padre-hijos')
    private async crearPadreHijos(
        @Body('padre') padre: PadreEntity,
        @Body('hijos') hijos: HijoEntity [],
    ) {
        return await this._padreService.registrarPadreConHijosTransaccion(
            padre,
            hijos
        )
    }
```
* En el `padre.service.ts` se crea el método que ejecuta la transación. Todo lo que ejecuta en una transacción debe ejecutarse en un callback.
1. Crear el método y tiparlo:
```typescript 
async registrarPadreConHijosTransaccion (padre: PadreEntity, hijos: HijoEntity[])
        : Promise<{mensaje: string, error: boolean, statusCode: number}> {}
```
2. Establecer la sintaxis dentro del método:
```typescript 
return getManager('default')
            .transaction(
                async (transaccionEntityManager: EntityManager) => {}
```
3. Realizar la funcionalidad dentro del callback de transacción. Es necesario usar `try()` `catch()` y primero declarar los repositorios que se van a usar con el transaccionEntityManager, sino la transacción no funcionará correctamente.
```typescript 
try{
    const padreRepository = transaccionEntityManager.getRepository(PadreEntity);
    const hijoRepository = transaccionEntityManager.getRepository(HijoEntity);
    const padreCreado = await padreRepository.save(padre);
    const hijosDelPadre = hijos.map(
        (hijoParametro: HijoEntity) => {
            hijoParametro.padre = padreCreado;
            return hijoParametro;
        }
    );
    await hijoRepository.save(hijosDelPadre);
    return {
        mensaje: 'Padre e hijos creados correctamente',
        error: false,
        statusCode: HttpStatus.CREATED,
    }
} catch (error) {
    console.error(
        {
            error,
            mensaje: 'Error al crear padre e hijos',
            data: {
                padre,
                hijos
            },
        },
    );
    throw new InternalServerErrorException({
        mensaje: 'Error del servidor',
    });
}
```
Este algoritmo primero declara los repositorios a usarse, guardar al padre enviado por parámetro usando el repositorio declarado, a cada hijo le asigna el padre creado y finalmente guarda a los hijos.

El método completo:
```typescript 
async registrarPadreConHijosTransaccion (padre: PadreEntity, hijos: HijoEntity[])
        : Promise<{mensaje: string, error: boolean, statusCode: number}> {
        return getManager('default')
            .transaction(
                async (transaccionEntityManager: EntityManager) => {
                    try{
                        const padreRepository = transaccionEntityManager.getRepository(PadreEntity);
                        const hijoRepository = transaccionEntityManager.getRepository(HijoEntity);
                        const padreCreado = await padreRepository.save(padre);
                        const hijosDelPadre = hijos.map(
                            (hijoParametro: HijoEntity) => {
                                hijoParametro.padre = padreCreado;
                                return hijoParametro;
                            }
                        );
                        await hijoRepository.save(hijosDelPadre);
                        return {
                            mensaje: 'Padre e hijos creados correctamente',
                            error: false,
                            statusCode: HttpStatus.CREATED,
                        }
                    } catch (error) {
                        console.error(
                            {
                                error,
                                mensaje: 'Error al crear padre e hijos',
                                data: {
                                    padre,
                                    hijos
                                },
                            },
                        );
                        throw new InternalServerErrorException({
                            mensaje: 'Error del servidor',
                        });
                    }
                }
            )
    }
```

## Casos de prueba

Los casos de prueba se detallan en el siguiente archivo: [Aquí](documentación/casos-prueba.md)